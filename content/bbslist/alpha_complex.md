---
title: "Alpha Complex"
telnet: "telnet://alphacomplex.us:2323"
ssh: "ssh://alphacomplex.us:2222"
www: "https://alphacomplex.us"
gopher: "gopher://alphacomplex.us:7070"
screenshot: "/images/bbs_sshots/alpha.png"
sysops: ["aLPHA"]
---
FTNs, Games, Art -- and an old-skool Paranoia RPG theme! Check out aLPHA comPLEX -- Trust The Computer.  The Computer is Your Friend!