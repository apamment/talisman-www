---
title: "Version Art"
layout: "single"
---

Originally aim was to do a new `login.ans` art file for each release, but that quickly became too much work.

Here's the artwork up until I stopped...

<div class="artwork" style="background-color: #000; text-align: center">
<span id="talismanV01"></span>
<span id="talismanV02"></span><br />
<span id="talismanV03"></span>
<span id="talismanV04"></span><br />
<span id="talismanV05"></span>
<span id="talismanV06"></span><br />
<span id="talismanV07"></span>
</div>

<script src="/scripts/ansilove.js" type="text/javascript" charset="utf-8"></script>        
<script type="text/javascript">
var controller, retina;


AnsiLove.render("/artwork/talismanv01.ans", function (canvas, sauce) {
    document.getElementById("talismanV01").appendChild(canvas);
}, {"bits": "9", "thumbnail": 3});

AnsiLove.render("/artwork/talismanv02.ans", function (canvas, sauce) {
    document.getElementById("talismanV02").appendChild(canvas);
}, {"bits": "9", "thumbnail": 3});

AnsiLove.render("/artwork/talismanv03.ans", function (canvas, sauce) {
    document.getElementById("talismanV03").appendChild(canvas);
}, {"bits": "9", "thumbnail": 3});

AnsiLove.render("/artwork/talismanv04.ans", function (canvas, sauce) {
    document.getElementById("talismanV04").appendChild(canvas);
}, {"bits": "9", "thumbnail": 3});

AnsiLove.render("/artwork/talismanv05.ans", function (canvas, sauce) {
    document.getElementById("talismanV05").appendChild(canvas);
}, {"bits": "9", "thumbnail": 3});

AnsiLove.render("/artwork/talismanv06.ans", function (canvas, sauce) {
    document.getElementById("talismanV06").appendChild(canvas);
}, {"bits": "9", "thumbnail": 3});

AnsiLove.render("/artwork/talismanv07.ans", function (canvas, sauce) {
    document.getElementById("talismanV07").appendChild(canvas);
}, {"bits": "9", "thumbnail": 3});

</script>

