---
title: "About"
Layout: "single"
---

# About Talisman

Talisman is a telnet style bulletin board system for Linux & Windows. It is a brand new system mixing the best bits of my previous BBS systems. Talisman is opensource and licensed under the GNU GPLv3.

## Features (So Far)

  * Squish Message bases with FTN support via HPT & Binkd
  * Door32.sys, door.sys, chain.txt dropfile support for Doors.
  * Using INI & TOML configuration formats.
  * Works on Windows 10 and Linux.
  * IP Blocklist / Passlist support, with auto blocking.
  * Configurable security level time outs & time limits.
  * File Conferences / Areas, External Protocols & Archivers.
  * SSH Support with automatic key generation.
  * QWK-E & Bluewave offline mail.
  * Private Email Support.
  * LUA Scripting.
  * Support for Terminal Sizes greater than 80x25.
  * Built in Full Screen Editor, with option of External (QuickBBS) Editor
  * Native FTN, WWIVnet and QWK networking support.

## Download

  * [Latest Releases Here](https://talismanbbs.com/releases/)
  * [Git Repo](https://gitlab.com/apamment/talisman/)
