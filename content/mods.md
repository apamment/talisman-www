---
title: "Mods"
layout: "Single"
---

This page lists any third party mods and programs that may be of interest to
talisman sysops.

## Auto-Sig Editor (by Alpha)

![Signature Editor](/images/autosig.png)

This simple, go-based program allows you to add an Auto Sig editor to your 
Talisman BBS menus -- as a door.

[Github Page](https://github.com/robbiew/go-autosig-talisman)
