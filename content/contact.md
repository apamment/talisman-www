---
title: "Contact"
layout: "Single"
---
# Sorry..

Due to the massive amounts of spam and the limited submissions per month getform.io allows me to have
this contact form was essentially made useless.

If you need to contact me, please do so via one of my BBSes.
