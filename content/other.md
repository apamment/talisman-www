---
title: "Other Stuff"
Layout: "single"
---

# Magiterm

Magiterm is an SDL2 based SSH / Telnet client for Bulletin Board Systems. It supports basic ANSI, extended pallets (8 bit and 24 bit), font loading and font switching, sixel, ZModem and more.

Magiterm is available as a Win32 installer, packages for linux and source code for other OSes, it is known to compile on Windows 10, Linux, FreeBSD, NetBSD, Haiku and macOS.

 * Win32 [Download](https://talismanbbs.com/downloads/MagiTerm/) ([Alternate](http://members.iinet.net.au/~apamment/))
 * Linux Package [Repositories](https://software.opensuse.org/package/magiterm)
 * [Source Code](https://gitlab.com/magickabbs/MagiTerm)

# TitanMail

TitanMail is a QT based QWK offline mail reader. It is available as a win32 installer and source code for other operating systems.

 * Win32 [Download](https://talismanbbs.com/downloads/TitanMail/) ([Alternate](http://members.iinet.net.au/~apamment/))
 * [Source Code](https://gitlab.com/apamment/titanmail)

# Gophari

Gophari is a wxWidgets based Gopher client. It is available for win32 as an installer and source code for other operating systems.

 * Win32 [Download](https://talismanbbs.com/downloads/Gophari/)
 * [Source Code](https://gitlab.com/apamment/gophari)

