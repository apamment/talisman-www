---
title: "TalismanBBS.com"
date: 2021-11-17T16:30:53+10:00
---
Hi all, as you may have noticed in the past few weeks, the talisman website
has been spread out across two domains, www.talismandocs.com and 
www.talismanbbs.com.

talismandocs.com is hosted by aLPHA, who stepped up when I was having trouble
paying for a server to host the docs / releases on. aLPHA bought a domain,
and put the docs up and has been paying for the server for the last several
months.

I am extremely grateful for this, it has been a real help. However just 
recently I spun up a new server to offer dynamic dns to talisman sysops, and
it seemed to me that it would be best to put all the docs on the one site,
and save aLPHA from the continuing cost when it was no longer really
necessary.

So, in short, talismanbbs.com is the home for Talisman BBS again,
talismandocs.com might point here in the near future, but I imagine it will
eventually go away.

aLPHA, thankyou for all your support this year, both financially with
hosting the site, and your continued enthusiasm and assistance with the
Talisman BBS project :)