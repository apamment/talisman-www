---
title: "Talisman v0.39 Available"
date: 2022-06-18T14:04:04+10:00
draft: false
---
Talisman v0.39 is out now!

  * Support for new user password. Incase you want to run a private BBS.
  * Support for font loading on login, fonts definable in [data/fonts.toml](/docs/fontstoml)
  * Support for sending sixel images from gfiles.

Talisman will determine if a terminal supports sixel or fonts or both, either by testing 
the term type to see if it's magiterm, or using a cterm device capability string for syncterm.

Other terminals that support these features may not be detected if they do not support the cterm
string. See [cterm.txt](https://gitlab.synchro.net/main/sbbs/-/blob/master/src/conio/cterm.txt) under 
`CSI = Ps n (CTSMRR)`.

