---
title: "Talisman v0.33 Available"
date: 2021-11-23T12:38:04+10:00
draft: false
---
Talisman v0.33 is out now!

 * Improved logging when loading invalid toml files.
 * Support for @USERNAME@ in login menu data
 * New "Top" user attributes and script functions
 * utf8 to cp437 conversion. (converts utf8 in some toml strings, strings.dat and utf8 messages)
 * Reply to Netmail function.
 * @MSGCONF@ available in fsr_header_*
 * Fixes for file listing descriptions.
 * Fix for toolbelt when importing files.na
 