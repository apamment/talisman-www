---
title: "Talisman v0.34 Available"
date: 2021-11-28T16:59:04+10:00
draft: false
---
Talisman v0.34 is out now!

 * Auto detection of UTF-8 Terminals. Converts appropriatly
 * Misc bug fixes.

NOTE: 
 * Talisman does not accept UTF-8 input. 
 * Doors under Windows are not converted to UTF-8.
