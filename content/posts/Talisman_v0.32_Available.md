---
title: "Talisman v0.32 Available"
date: 2021-11-17T09:35:04+10:00
draft: false
---
Talisman v0.32 is out now!

 * Fixes for postie working as a point (Thanks Sneaky!)
 * Several new scripting support functions
 * Initial support for Synchronet style HEADERS.DAT in qwkie
 * New script hook, can be run from fsr_header_* gfiles
 * Fix for internode chat causing a crash

