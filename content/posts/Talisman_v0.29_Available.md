---
title: "Talisman v0.29 Available"
date: 2021-11-02T12:50:50+10:00
draft: false
---
Talisman v0.29 is out now, with reworked lastread pointers and message
deletion.

It's important you recompile all your executables this time if you're on
linux, as the Squish stuff has changed.

Windows can of course just install over the top of their old installation.

To delete a message, you need to either have deletion enabled for your
security level, or be in a message area that allows deletion for your
security level.

sec_levels have the additional flags:

    can_delete_own_msgs = false
    can_delete_msgs = false

these default to false.

Message areas have the optional additional flags

    delete_sec_level = 99
    delete_own_sec_level = 99

These default to -1 or NO ONE.

So, if you only want sysops to be able to delete messages, you would
enable it in seclevels.ini for the sysop security level. If you also want
level 25+ to be able to delete their own messages in a particular base,
you would add delete_own_sec_level = 25 to that message base.
