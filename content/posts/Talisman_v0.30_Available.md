---
title: "Talisman v0.30 Available"
date: 2021-11-07T13:46:50+10:00
draft: false
---
Talisman v0.30 is out now.

 * Includes various bugfixes to postie, with added debug logging.
 * Includes toolbelt “adpost” command and “pack” command, for posting text files to message bases and packing message bases respectively.
 * Fixed file search, fixes for Qwkie.
 * Added optional newuser.lua to be triggered when a new user signs up.
 * bbs_set_user_attribute added to lua scripting to set attributes for users.
 * Added optional scan hook and toss hook to message areas in postie.
 * Reworked events, so they start based on the start of the month (or an optional start day/time), instead of when Servo is started.
 * Log files now include the pid of the logging process.

