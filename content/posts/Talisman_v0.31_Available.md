---
title: "Talisman v0.31 Available"
date: 2021-11-12T22:41:42+10:00
draft: false
---
Talisman v0.31 is out now

 * New split screen chat option between nodes.
 * Built in RLogin and Telnet clients, accessible via menu and scripting
 * Full screen ANSI editor accessible via scripting.
 * Upload / Download available by scripting.
 * Pruning message bases with toolbelt.
 * Postie bug fixes and improvements.
