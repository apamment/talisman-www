---
title: "Top 5"
download: "/downloads/TALTOP5.ZIP"
screenshot: "/images/sshots/top5.png"
authors: ["apam"]
---
Top 5 users script, lists the top 5 callers, uploaders, downloaders, message posters and game players.
Excludes the sysop.
