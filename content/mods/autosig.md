---
title: "AutoSig"
download: "https://github.com/robbiew/go-autosig-talisman"
screenshot: "/images/sshots/autosig.png"
authors: ["aLPHA"]
---
This simple, go-based program allows you to add an Auto Sig editor to your 
Talisman BBS menus -- as a door.
