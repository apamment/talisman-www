---
title: "Avatars"
download: "/downloads/TALAV07.ZIP"
screenshot: "/images/sshots/avatars.png"
authors: ["apam"]
---
### Synchronet style avatars for Talisman!

 * BIN style avatar sets
 * Sharing via networked message bases
 * Import / Export your avatar
 * Edit your avatar
