---
title: "Getting Started on Windows"
layout: "Single"
---

# Installation

The initial install of Talisman on windows is easy, and is just a matter of downloading an installer and running through the install wizard.

## Windows Installer Step 1 - Details
![Windows Step 1](/images/win_step_1.png)

In this step you are asked to provide some details about the BBS you want to run.

  * **BBS name** - this is what you want to call your BBS.
  * **Sysop Username** - this will be your username / handle on the BBS.
  * **Location** - this is the general location of the BBS, used in QWK packets.
  * **QWKID** - this is the QWKID of your system, it must be 8 characters or less and uppercase.
  * **BBS Hostname** - this is the public facing hostname of your system.

Once these are all filled in, click *Next*.

## Windows Installer Step 2 - License

![Windows Step 2](/images/win_step_2.png)

In this step you are prompted to accept the GNU GPLv3 License agreement that this software is licensed under. If you accept, click *I accept the agreement* then click next.

## Windows Installer Step 3 - Destination

![Windows Step 3](/images/win_step_3.png)

In this step you select the location you wish to install the BBS. It is recommended that the path you install into has no spaces.

## Windows Installer Step 4 - Start Menu

![Windows Step 4](/images/win_step_4.png)

In this step you choose where in the start menu to put the icons.. the default location should do.

## Windows Installer Step 5 - Desktop Icons

![Windows Step 5](/images/win_step_5.png)

In this step you choose if you want desktop icons or not.

## Windows Installer Step 6 - Install!

![Windows Step 6](/images/win_step_6.png)

This step summarizes your choices, and is now ready to install. Click *Install* to install!

## Windows Installer Step 7 - Done!

![Windows Step 7](/images/win_step_7.png)

Click *Finish* to exit the installer.

# What now?

Now that talisman is installed, you can start it by clicking the *Talisman BBS (Servo)* icon in the start menu (or on the desktop). You can log in and create your sysop account - remember to use the username you entered previously in the installer.

You will probably want to configure your BBS, use a text editor like notepad to edit *talisman.ini* and any of the *\*.toml* files. See the documentation for indepth detail on each file and option.