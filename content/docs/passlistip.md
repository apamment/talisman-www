---
title: "Allowed IP Addresses"
layout: "Single"
---

*passlist.ip* contains a list of ips that are immune to blocking rules.

Place the IP addresses of your friends in here so they don't accidently get blocked.