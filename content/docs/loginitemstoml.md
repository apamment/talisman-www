---
title: "Login Customization"
layout: "Single"
---

**loginitems.toml** is an array of items that are performed after a user logs in and before the main menu is shown.

### Example

    [[loginitem]]
    clear_screen = true
    pause_after = true
    command = "SENDGFILE"
    data = "login"
     
    [[loginitem]]
    clear_screen = true
    pause_after = false
    command = "BULLETINS"

### Login Item Array

The login items are an array of "loginitem" they contain the following attributes.

  * **command** This is the menu COMMAND, commands will be listed later. *Required*
  * **data** This is a data field that some COMMANDs require. *Depends on command*
  * **clear_screen** True if the screen is to be cleared before performing the command *Optional, default FALSE*
  * **pause_after** True if a pause prompt should be shown after performing the command *Optional, default FALSE*
  * **sec_level** The minimum seclevel the user must be for this command to perform. *Optional, default 0 - Everyone*

### Menu Commands

  * **MAILSCAN** Does a "New Mail" scan.
  * **LAST10** Displays the last 10 callers.
  * **EMAILCHECK** Checks for new email and prompts to read them if there are any.
  * **RUNDOOR** Runs a door, **DATA** is the full path and filename of a script to run, the script is passed a node number on Linux and a node number and socket handle on Windows.
  * **BULLETINS** Displays the bulletins menu.
  * **RUNSCRIPT** Runs a script, **DATA** is the name of the script to run.
  * **MSGREADNEW** Reads all new messages.
  * **NEWFILES** Scan for new files since last login.
  * **QUICKLOGIN** Prompt the user if they want a "Quick Login" if they say yes, bypass all following login items.
