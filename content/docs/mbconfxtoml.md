---
title: "Message Area Config"
layout: "Single"
---

*mb_confx.toml* (where mb_confx is defined in [msgconfs.toml](/docs/msgconftoml)) contains an array of Message Area definitions.


### Example

    [[messagearea]]
    name = "General Discussion"
    file = "general"
    read_sec_level = 10
    write_sec_level = 10
    aka = "637:1/101"
    wwivnode = 101
    netmail = false
    qwk_base_no = 100
    real_names = true
    delete_sec_level = 99
    delete_own_sec_level = 99

### The Attributes

  * **name** This is the name of the area as displayed to users. *Required*
  * **config** This is the basename of the squish file set relative to the message path. *Required*
  * **read_sec_level** This is the security level required to access the area *Optional (default 10)*
  * **write_sec_level** This is the security level required to post to the area *Optional (default 10)*
  * **aka** This is the origin address used for FTN networks *Optional (No default)* **CAN ONLY BE SET IF WWIVNODE IS UNSET**
  * **wwivnode** The wwivnet node number for this base **CAN ONLY BE SET IF AKA IS UNSET**
  * **netmail** True/False if this base is a netmail base (or a wwivnet email base) *Optional (Default false)*
  * **qwk_base_no** QWK base number. *Optional (Default -1 / Disabled)*
  * **real_names** Whether real names are required for this base *Optional (Default false)*
  * **delete_sec_level** What sec level is required to delete any messages in this area *Optional (Default NO ONE)*
  * **delete_own_sec_level** What sec level is required to delete messages in this area that the user has authored *Optional (Default NO ONE)*
