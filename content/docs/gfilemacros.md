---
title: "G-File Inline Macros"
layout: "Single"
---

Macros are special tags in gfiles that are replaced with relevent data. Most
macros now support size setting by appending the macro name with '#' up until
the end of the size you want.

eg. @MSGCONF########################################@

The following macros are currently available for use in gfiles:

#### These macros DO support size setting

  * @MSGCONF@ - Displays the user's current Message Conference.
  * @MSGAREA@ - Displays the user's current Message Area.
  * @FILECONF@ - Displays the user's current File Conference.
  * @FILEAREA@ - Displays the user's current File Area.
  * @VERSION@ - Displays the Talisman version in use.
  * @TIMELEFT@ - Displays the user's time remaining online.
  * @SECLEVEL@ - Displays a description of the user's security level.
  * @ULOCATION@ - Displays the user's location (as entered).
  * @UNAME@ - Displays the user's login name
  * @UFULLNAME@ - Displays the user's full name.
  * @PHLOGURL@ - Displays the user's personal Phlog (Gopher) URL.

#### These macros DO NOT support size setting

  * @RUNSCRIPT:scriptname@ - Displays the output of the lua script "scriptname".
  * @NOPAUSE@ - Disables pausing for this gfile.
  * @PAUSE@ - Pauses display with a pause prompt.
  * @FONT:slotno@ - Changes the default font the font in "slotno" slot.
  * @FONTBOLD:slotno@ - Changes the font for bold text to the font in "slotno".
  * @FONTBLINK:slotno@ - Changes the font for blinking text to the font in "slotno".
  * @FONTBOLDBLINK:slotno@ - Changes the font for bold and blinking text to the font in "slotno".
  * @SIXEL:/path/to/image.sixel@ - Display a sixel image here.