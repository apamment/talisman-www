---
title: "Documentation"
menu: "Docs"
layout: "single"
---
# Current Version v0.42

This documentation has been updated for Talisman v0.42, Postie v0.12 and Binki v0.4

# Configuration

 * Basic System Configuration: [talisman.ini](/docs/talismanini)
 * Events to perform on user login: [data/loginitems.toml](/docs/loginitemstoml)
 * Message Conference Configuration: [data/msgconfs.toml](/docs/msgconftoml)
 * Message Base Configuration: [data/mb_confx.toml](/docs/mbconfxtoml)
 * File Conference Configuration: [data/fileconfs.toml](/docs/fileconftoml)
 * File Base Configuration: [data/fb_confx.toml](/docs/fbconfxtoml)
 * Transfer Protocol Configuration: [data/protocols.toml](/docs/protocolstoml)
 * Security Level Definitions: [data/seclevels.toml](/docs/seclevelstoml)
 * File Archiver Configuration: [data/archivers.toml](/docs/archiverstoml)
 * Bulletin Configuration: [data/bulletins.toml](/docs/bulletinstoml)
 * Disallowed Usernames: [data/trashcan.txt](/docs/trashcantxt)
 * IP Addresses to Block from Servo: [data/blocklist.ip](/docs/blocklistip)
 * IP Addresses to Pass from Servo: [data/passlist.ip](/docs/passlistip)
 * IP Addresses to Allow Multiple Instances: [data/multiallow.ip](/docs/multiallowip)
 * Postie (Mail/File Tosser/Scanner): [data/postie.toml](/docs/postietoml)
 * WWIV Networking Configuration: [data/falcon.toml](/docs/falcontoml)
 * QWK Networking Configuration: [data/qwkie.toml](/docs/qwkietoml)
 * Servo Event Configuration: [data/events.toml](/docs/eventstoml)
 * Theme Configuration: [data/themes.toml](/docs/themestoml)
 * Binkp Server Configuration [data/binki.toml](/docs/binkitoml)
 * Font Configuration: [data/fonts.toml](/docs/fontstoml)

# G-Files

G-Files are ANSI/ASCII graphics that are used to display menus and such, depending on whether ANSI is detected, a user will either be shown the ANSI (.ans) or ASCII (.asc) version.

Seperate ANSI and ASCII graphics may be made for different terminal sizes, by naming the file (for example) mygfile.132x37.ans where 132x37 is the screen size to send. If no matching screen size is found, the default .ans (width no size specification) will be sent.

ANSI and ASCII files may contain SAUCE records, they will be ignored. They may also contain inline macros.

## Animated Pause Prompts

Single line animated pause prompts are supported, and live in *gfiles/pause* directory. Talisman will select a random pause prompt file from this directory if a user has ANSI enabled. A pause prompt file is in the format:

    125
    PaUSE
    PAuSE
    PAUsE
    PAuSE

This is a basic pause prompt with four frames and a transision speed of 125 milliseconds. The first line is the speed, the following line are frames. Each line can contain ANSI codes or PIPE codes for colour.

 * [Inline Macros](/docs/gfilemacros)

# Customizing Strings

Most strings can be customized in talisman using `data/strings.dat` instead of modifying the code. This makes it easier to upgrade without having to redo all your changes everytime.

The format of the `data/strings.dat` file pairs of `msgid`s and `msgstr`s, example:

    msgid "LOGIN: "
    msgstr "Your UserName: "

This will change where the BBS has the content `LOGIN: ` in it's print_f command to `Your UserName: `

It's important to make sure any format specifiers, such as %d, %s etc, are in your msgstr in the same order as in the print_f statement.

The `data/strings.dat` file can also contain comments (lines beginning with `#`)

MsgStrs can also display gfiles by using a special tag: `@gfile:somegfile@` will display a gfile from your gfiles directory called somegfile, example:

    msgid "LOGIN: "
    msgstr "@gfile:login_banner@\r\nLOGIN: "

This will change the LOGIN: prompt to display a login_banner file, then a newline, then `LOGIN: `

# Menus

 * [Menu Files](/docs/menus)

# Scripting

 * [Lua Scripting Commands](/docs/lua)

# Utilities

 * [Toolbelt](/docs/toolbelt)

# Gopher

 * [GopherMap](/docs/gophermap)

# Quick Start

 * [Getting started on Debian (Manual)](/docs/debian)
 * [Getting started on Windows 10](/docs/windows)

# Updating

## Windows

Updating on Windows is usually just a matter of downloading the installer and running it.

## Linux

To update linux, if not using git, you should download the tarball, and extract it in your source directory..

    cd /home/user/bbs/source
    tar xf /home/user/Downloads/talisman-v0.30.tar.gz

Next, change to the build directory and "re-make" each executable

    cd ../build/servo
    make

    cd ../talisman
    make

    cd ../toolbelt
    make

    cd ../postie
    make

    cd ../gofer
    make

    cd ../qwkie
    make

    cd ../falcon
    make

    cd ../binki
    make

Your old executables will be replaced with freshly built ones, and your symlinks will point to the new executables.

