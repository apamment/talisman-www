---
title: "Menu Files"
layout: "Single"
---

Menus are TOML files located in the Menus Path. The Root Menu is called first, then all menus exist as a tree from that.

### Example

    [menu]
    gfile = "mainmenu"
    
    [[menuitem]]
    command = "SUBMENU"
    hotkey = "M"
    data = "message"
    
    [[menuitem]]
    command = "SUBMENU"
    hotkey = "D"
    data = "doors"

### The Menu section

The menu section occurs just once,
  
  * **gfile** The gfile to display for the menu *Required*
  * **prompt** Override the default prompt with this. You can use pipe codes in here. Can also be a GFILE, use eg: @@GFILE:somegfile@@ *Optional, default none*

### Menu Item Array

The menu items are an array of "menuitem" they contain the following attributes.

  * **command** This is the menu COMMAND, commands will be listed later, in these examples the SUBMENU command is called which displays a new menu. *Required*
  * **hotkey** This is the character (or string) a user will have to enter to activate the item. *Required*
  * **data** This is a data field that some COMMANDs require. *Depends on command*
  * **sec_level** This is the required sec_level for the menu option to work. *Optional*

### Menu Commands

  * **GOODBYE** This logs out of the system
  * **SUBMENU** This opens a new menu, **DATA** is the name of the menu TOML file.
  * **PREVMENU** This returns to the previous menu (The one that called submenu).
  * **LISTCONFS** Lists mail conferences.
  * **LISTAREAS** Lists mail areas, in the current conference.
  * **LISTMSGS** Lists the messages in the current area.
  * **POSTMSG** Posts a message into the current area.
  * **MAILSCAN** Does a "New Mail" scan.
  * **LAST10** Displays the last 10 callers.
  * **RUNDOOR** Runs a door, **DATA** is the full path and filename of a script to run, the script is passed a node number on Linux and a node number and socket handle on Windows.
  * **SYSINFO** Displays the system information.
  * **SETTINGS** Shows the user settings menu.
  * **POSTEMAIL** Post an email to another user.
  * **LISTEMAIL** List your email.
  * **FEEDBACK** Send a message to the user listed as the sysop in the [talisman.ini](/docs/talismanini) file.
  * **LISTUSERS** List all the users on the system.
  * **BULLETINS** Displays the bulletins menu.
  * **FILECONFS** Lists file conferences.
  * **FILEAREAS** Lists file areas in the current conference.
  * **LISTFILES** Lists files in the current area.
  * **DOWNLOAD** Downloads the user's tagged files.
  * **CLEARTAGGED** Clears a user's tagged file list.
  * **RUNSCRIPT** Runs a script, **DATA** is the name of the script to run.
  * **MSGSEARCH** Perform a search of the message bases.
  * **MSGREADNEW** Reads all new messages.
  * **MSGUPDATELR** Updates last read message pointers.
  * **MSGSUBAREAS** Subscribe / Unsubscribe from areas.
  * **QWKUP** Upload QWK Packet.
  * **QWKDOWN** Download QWK Packet.
  * **NEWFILES** Scan for new files since last login.
  * **FILESEARCH** Search for New Files.
  * **NLBROWSE** Browse nodelists.
  * **NODEMSG** Inter-Node Messaging.
  * **NEXTMAILCONF** Change to next message conference.
  * **PREVMAILCONF** Change to previous message conference.
  * **NEXTFILECONF** Change to next file conference.
  * **PREVFILECONF** Change to prev file conference.
  * **NEXTMAILAREA** Change to next message area.
  * **PREVMAILAREA** Change to previous message area.
  * **NEXTFILEAREA** Change to next file area.
  * **PREVFILEAREA** Change to prev file area.
  * **PHLOGNEW** Add an article to user's phlog.
  * **PHLOGMANAGE** Manage the user's phlog articles.
  * **INDEXREADER** Run the Index Message Reader.
  * **BWAVEUP** Upload Bluewave Packet.
  * **BWAVEDOWN** Download Bluewave Packet.
  * **RLOGIN_IP4** Connect to a remote RLOGIN server via IPv4 **DATA** is HOST=somehostorip,PORT=513,LUSER=someuser,RUSER=someuser,TERM=termtype only HOST is required, port defaults to 513, LUSER and RUSER default to the user's handle and TERM defaults to nothing. NOTE: there are no spaces next to commas or equals signs.
  * **RLOGIN_IP6** Same as RLOGIN_IP4 but via IPv6.
  * **TELNET_IP4** Connect to a remote TELNET server via IPv4 **DATA** is HOST=somehostorip,PORT=23 only HOST is required, port defaults to 23 NOTE: there are no spaces next to commas or equals signs.
  * **TELNET_IP6** Same as TELNET_IP4 but via IPv6.

