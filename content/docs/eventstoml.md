---
title: "Events Config"
layout: "Single"
---

Servo uses events.toml in the data directory to schedule both timed and file triggered events.

An example of polling a network every 15 minutes:

    [[event]]
    name = "Poll a Network"
    start = "Tue,13:30"
    interval = 15
    exec = "/usr/local/sbin/binkd -p -P 99:999/1 /home/user/bbs/ftn/binkd.cfg"

An example of scanning mail when mail.sem is modified:

    [[event]]
    name = "Scan Mail"
    watchfile = "/home/user/bbs/mail.sem"
    exec = "/home/user/bbs/postie_scan.sh"

In this example `postie_scan.sh` would be a script that changes to the bbs directory, and runs
`postie scan`.

Note: events can be timed OR file triggered, not both. If you want both, setup two events.

## The [[event]] Array

The event array is an array of events to queue in servo.

 * *name* this is a friendly name for the event. (The Name "SILENT" will stop it showing in the servo output.)
 * *interval* this is the interval in minutes for an event to run. Specifying this sets it as a timed event.
 * *start* this is the offset to use for the start of a timed event. It is optional. It is in the format "Day,HH:MM"
 * *watchfile* this is the file to watch for changes. Specifying this sets it as a file triggered event.
 * *exec* this is the command to execute when the event is triggered.

NOTE: Servo can handle event intervals up to and including one week. Any longer, and it is suggested you use cron or windows scheduler.
