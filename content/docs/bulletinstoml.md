---
title: "Bulletins Configuration"
layout: "Single"
---

*bulletins.toml* contains an array of bulletin definitions.

### Example

    [[bulletin]]
    name = "Talisman BBS Information"
    file = "bulletin0"
    hotkey = "1"
    sec_level = 10

### The Attributes

  * **name** This is the name of the bulletin as displayed to users. *Required*
  * **file** This is the gfile to display to users (ie. the actual bulletin). *Required*
  * **hotkey** The character (or string) to show the bulletin. *Required*
  * **sec_level** The secutiry level required to view the bulletin. *Optional, Default 10*