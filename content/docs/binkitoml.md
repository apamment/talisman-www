---
title: "Binki Config"
layout: "Single"
---

# WARNING: Binki is currently experimental!

NOTE: Besides binki.toml, binki also uses [talisman.ini](/docs/talismanini), it must be run from the bbs root directory.

*binki.toml* contains information for the binkp mailer "Binki".

example:

    [binki]
    inbound = "C:/bbs/bink/in"
    secure_inbound = "C:/bbs/bink/in_sec"
    outbound = "C:/bbs/bink/out"
    default_zone = 637
    semaphore = "C:/bbs/data/mailin.sem"
    
    [[address]]
    address = "637:1/999"
    domain = "happynet"
    
    [[address]]
    address = "21:1/999"
    domain = "fsxnet"
    
    [[link]]
    domain = "happynet"
    address = "637:1/100"
    outbox = "C:/bbs/bink/ob1"
    password = "SECRET"
    cram-md5 = true
    host = "happylandbbs.com"
    port = 24557
    
    [[link]]
    domain = "fsxnet"
    address = "21:1/100"
    outbox = "C:/bbs/bink/ob2"
    password = "SECRET"
    cram-md5 = true
    host = "ipv4.agency.bbs.nz"
    port = 24556

## The Attributes

 * **inbound** The path to the non-secure inbound, binki will place files here from un-authenticated nodes. (*Required*)
 * **secure_inbound** The path to the secure inbound, binki will place files here from authenticated nodes. (*Required*)
 * **outbound** The path to the outbound for the default zone. (*Required*)
 * **default_zone** The default zone (*Required*)
 * **semaphore** A semaphore that binki will create upon receiving files (*Required*)

## The Address Array

This is an array of your system's addresses.

 * **address** An AKA for your system. (*Required*)
 * **domain** The domain for this AKA. (*Required*)

## The Link Array

This is an array of links your system connects to.

 * **domain** The Domain for this Link (*Required*)
 * **address** The AKA for the link (*Required*)
 * **outbox** An outbox for the link (*Required*)
 * **password** The password for the link (*Required*)
 * **cram-md5** Should CRAM-MD5 authentication be required? (*Required*)
 * **host** The host part of the binkp address for the link (*Required*)
 * **port** The port number for the binkp address for the link (*Optional, default 24554*)

# Extra steps for a working setup

In addition to creating a binki.toml file, you will also need to enable binkp in servo, by setting the port to something other than -1 in [talisman.ini](/docs/talismanini).

You will also need to create two [events](/docs/eventstoml) for servo,

 1. An event to scan the outbounds for outgoing files.

 example:

    [[event]]
    name = "SILENT"
    exec = "binki -O"
    interval = 1

 2. An event to process mail / files upon binki creating a semaphore.

 example:

    [[event]]
    name = "Receive Network Mail"
    exec = "C:/bbs/postie_toss.bat"
    watchfile = "C:/bbs/data/mailin.sem"

# Polling nodes manually

Binki can poll nodes manually by running from the commandline:

example:

    binki -P 21:1/100@fsxnet
