---
title: "File Area Config"
layout: "Single"
---

*fb_confx.toml* (where fb_confx is defined in [fileconfs.toml](/docs/fileconftoml)) contains an array of File Area definitions.


### Example

    [[filearea]]
    name = "Miscellanious"
    database = "fb_misc"
    file_path = "dloads/general/misc"
    upload_sec_level = 10
    download_sec_level = 10
    delete_sec_level = 99

### The Attributes

  * **name** This is the name of the area as displayed to users. *Required*
  * **database** This is the sqlite database relative to the data path that stores file download information. *Required*
  * **file_path** This is the path to where the files are expected to reside (and uploaded to) *Required*
  * **upload_sec_level** This is the security level required to upload to the area *Optional (default 10)*
  * **download_sec_level** This is the security level required to upload to the area *Optional (default 10)*
  * **delete_sec_level** Security level required to delete files. *Optional (default -1 - NO ONE)*
