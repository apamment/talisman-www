---
title: "Fonts Config"
layout: "Single"
---

*fonts.toml* contains an array of available fonts.


### Example

    [[font]]
    slot = 100
    filename = "/home/andrew/bbs/fonts/antique.f16"

### The Attributes

  * **slot** The slot to use for this font. This should be unique, and greater than any built in fonts - default CP437 is 0, 100 is a good place to start. *Required*
  * **filename** The filename of the 8x16 font. *Required*