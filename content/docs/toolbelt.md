---
title: "Toolbelt Documentation"
layout: "Single"
---
Toolbelt is a command line utility for administering a Talisman BBS. Like Servo (and Talisman) it must be called from the root bbs directory.

The expected arguments are in the form:

    ./toolbelt COMMAND [args]

## Commands

### password

The password command changes a user's password. arguments are username and the new password.

eg.

    ./toolbelt password someuser supersecret

This will change the user "someuser"'s password to "supersecret".

### seclevel

The seclevel command changes a user's security level. Before changing a user's security level ensure that the new security level is defined in seclevels.toml. Arguments are username and the number representing the new security level.

eg.

    ./toolbelt seclevel someuser 15

This will change the user "someuser"'s security level to 15.

### uploadindex

The uploadindex command uploads files referenced by an index file, in FILES.BBS type format.

eg.

    ./toolbelt uploadindex /some/file/directory/FILES.BBS /some/file/directory /some/database.sqlite3

This will add files referenced in `/some/file/directory/FILES.BBS` to `/some/database.sqlite3` The files must be in `/some/file/directory`

The FILES.BBS format toolbelt recognizes is in the format, FILENAME DESCRIPTION with extended descriptions preceded by white space.

### uploadbulk

The uploadbulk command uploads files in a file directory that don't exist in the database. If the file is a recognized archive and contains a file_id.diz, it will import the description, otherwise, the file is uploaded with "No Description". It skips `files.bbs` files.

eg.

    ./toolbelt uploadbulk /some/file/directory /some/database.sqlite3

This will add files in `/some/file/directory` that are missing from the database `/some/database.sqlite3` into it. Files will be "Uploaded By" Unknown.

It is possible to specify a fourth argument, which will set the "Uploaded By" accordingly,

eg.

    ./toolbelt uploadbulk /some/file/directory /some/database.sqlite3 "File Uploader"

This is the same command as the previous, but the "Uploaded By" will be "File Uploader" instead of "Unknown".

### filetrim

The filetrim command trims missing files from file databases. It takes one argument, the file database to trim.

eg.

    ./toolbelt filetrim /some/database.sqlite3

### allfiles

The allfiles command generates a listing of all the files on the bbs. It takes 2 arguments, a security level and an output file

eg.

    ./toolbelt allfiles 10 allfiles.txt


### newfiles

The newfiles command generates a listing of all the files on the bbs newer than a specified date. It takes 3 arguments, a security level, a date and an output file

eg.

    ./toolbelt newfiles 10 2020.11.20 newfiles.txt


### nodelistp

Parse a nodelist into a database.

eg.

    ./toolbelt nodelistp somenet NODELIST.XXX data/nodelist.sqlite3

This command will parse the nodelist "NODELIST.XXX" with the domain somenet into data/nodelist.sqlite3. Talisman uses a single nodelist.sqlite3 in the data directory to get nodelist information.

### movefile

Move a file from one base to another.

eg.

    ./toolbelt movefile /path/to/srcfile.name /path/to/srcdatabase.sqlite3 /path/to/destfile.name /path/to/destdatabase.sqlite3

This command moves the source file which is referenced in `srcdatabase.sqlite3` to destfile.name to be referenced in `destdatabase.sqlite3`

### clearlrall

Clear all last read pointers, for all users, in all message bases.

eg.

    ./toolbelt clearlrall

### clearlruser

Clear all last read pointers for a user.

eg.

    ./toolbelt clearlruser apam

This will clear all last read pointers for user 'apam'

### clearlrbase

Clear all last read pointers for a message base.

eg.

    ./toolbelt clearlrbase hpy/gen

This will clear all last read pointers in the message base hpy/gen (under the messages directory) for all users.

### clearlrboth

Clear last read pointer for a specific user on a specific base.

eg.

    ./toolbelt clearlrbaseuser hpy/gen apam

This will clear apam's last read pointer in the base hpy/gen (under the messages directory).

### deleteuser

Delete a user

eg.

    ./toolbelt deleteuser apam

This will delete the user apam, including all their emails, phlogs, subscription lists, attributes and last read pointers.

### convertmsgna

Convert a FIDONET.NA style area list to TOML and outputted to the console.

eg.

    ./toolbelt convertmsgna -m FIDONET.NA 10 10 3:640/999 3:640/1 100 \[Fidonet\]

The first argument can be either -m or -p, -m is for outputting a message area TOML file, -p for a postie TOML fragment.

The second argument is the source file.

The third argument is the security level required to read the area.

The fourth argument is the security level required to post to the area.

The fifth argument is your AKA for this net.

The sixth argument is your uplink's AKA

The seventh argument is what number to begin assigning qwk ids, in this example it will start at 100 and increment for every area there is in the file.

The eighth argument is optional, and is a prefix to put ahead of the description of the area.

### convertfilena

Convert a FILEBONE.NA style file area list to TOML and outputted to the console.

eg.

    ./toolbelt convertfilena -f FILEBONE.NA 99 10 10 3:640/999 3:640/1 /home/bbs/dloads/fidonet true \[Fidonet\]

The first argument can be either -f or -p, -f is for a file area TOML file the -p is for a postie TOML fragment.

The second argument is the source file.

The third argument is the security level required to upload files to the area.

The fourth argument is the security level required to download files from the area.

The fifth argument is the security level required to view the file area listings.

The sixth argument is your AKA for this net.

The seventh argument is your uplink's AKA for this net.

The eighth argument is the root directory that subdirectories for the files will be created.

The ninth argument is either true or false, depending on if you want this command to create the missing directories.

The tenth argument is optional, and is a prefix to put ahead of the description of the area.

### adpost

Post a text file to a message base.

eg

    ./toolbelt adpost ad.ini

The first argument is an INI file describing the text file to be posted. The format is as follows:

    [main]
    Message Base = fsx/ads
    Origin Address = 21:1/151
    Tagline = The Grinning Cat - telnet://gcat.talismanbbs.com:11892/
    To = All
    From = Toolbelt
    Subject = Visit The Grinning Cat Today!
    Message File = gcat_ad.txt

*Message Base* is the filename of the message base you want to post to, relative to the message path.

*Origin Address* can either be an FTN address or a WWIV node number, depending on the base you're posting to, or it can be ommitted entirely for a local or QWK base.

*Tagline* used in FTN and WWIV message bases.

*To* who the message should be addressed to, default is "All"

*From* who the message should appear to be from, default is "Toolbelt"

*Subject* the subject of the message.

*Message File* the text file containing the content of the message.

NOTE: toolbelt does not touch the echomail semaphore, so running postie scan (or falcon scan) after posting with this tool is recommended (or "touch" the semaphore manually)

NOTE: Netmails are not supported.

### pack

Pack a Squish message base.

eg

    ./toolbelt pack fsx/ads

The first argument is the message base file, without extension and relative to the message base path.

### prune

Delete messages from a Squish message base.

eg

    ./toolbelt prune fsx/ads 500

The first argument is the message base file, without extension and relative to the message base path.

The second argument is the number of messages to leave in the message base.
