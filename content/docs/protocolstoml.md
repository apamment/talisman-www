---
title: "Protocol Config"
layout: "Single"
---

*protocols.toml* contains an array of transfer protocol definitions.


### Example

    [[protocol]]
    name = "Zmodem"
    upload_command = "rz -R"
    download_command = "sz -Y @FILELIST@"
    ssh_upload_command = "rz -R"
    ssh_download_command = "sz -Y @FILELIST@"
    batch = true
    prompt = false


### The Attributes

  * **name** This is the name of the protocol as displayed to users. *Required*
  * **upload_command** This is the upload command line, *Required* Macros include:
    * @SOCKET@ The socket handle 
    * @UPPATH@ The full path to upload to.
    * @FILENAME@ The name of the file to receive.
  * **download_command** This is the download command line, *Required* Macros include:
    * @SOCKET@ The socket handle.
    * @FILELIST@ A list of files to download.
    * @FILENAME@ A single file to download.
  * **ssh_upload_command** The same as upload command, but for ssh *Optional, defaults to upload_command*
  * **ssh_download_command** The same as download command, but for ssh *Optional, defaults to download_command*
  * **batch** Whether or not this protocol supports batch transfers (uses @FILELIST@) *Required*
  * **prompt** Whether or not this protcol requires the user to enter a filename before uploading. *Required*