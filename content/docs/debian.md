---
title: "Getting started on debian (Manual)"
layout: "single"
---

## Prerequesites

    sudo apt install build-essential cmake libsqlite3-dev libssl-dev lrzsz liblua5.3-dev libssh-dev libcurl4-openssl-dev

## Create Directories

    mkdir bbs
    mkdir bbs/source
    mkdir bbs/build
    mkdir bbs/build/talisman
    mkdir bbs/build/servo
    mkdir bbs/build/toolbelt
    mkdir bbs/build/postie
    mkdir bbs/build/gofer
    mkdir bbs/build/falcon        
    mkdir bbs/dloads
    mkdir bbs/dloads/general
    mkdir bbs/dloads/general/uploads
    mkdir bbs/dloads/general/misc
    mkdir bbs/scripts
    mkdir bbs/scripts/data
    mkdir bbs/data
    mkdir bbs/menus
    mkdir bbs/gfiles
    mkdir bbs/msgs
    mkdir bbs/logs
    mkdir bbs/temp
    mkdir bbs/gopher

## Unpack the Tarball

    cd bbs/source
    tar xzvf /path/to/Talisman-v0.32-source.tar.gz

## Build the source

### Build Servo

    cd ../build/servo
    cmake ../../source/talisman/Servo
    make

### Build Talisman

    cd ../../build/talisman
    cmake ../../source/talisman/Talisman
    make

### Build Toolbelt

    cd ../../build/toolbelt
    cmake ../../source/talisman/Toolbelt
    make

### Build Postie

    cd ../../build/postie
    cmake ../../source/talisman/Postie
    make

### Build Gofer

    cd ../../build/gofer
    cmake ../../source/talisman/Gofer
    make    

### Build Falcon

    cd ../../build/falcon
    cmake ../../source/talisman/Falcon
    make  

### Build Qwkie

    cd ../../build/qwkie
    cmake ../../source/talisman/Qwkie
    make  

### Build Binki

    cd ../../build/binki
    cmake ../../source/talisman/Binki
    make  


## Create Links

    cd ../../
    ln -s build/servo/servo
    ln -s build/talisman/talisman
    ln -s build/toolbelt/toolbelt
    ln -s build/postie/postie
    ln -s build/gofer/gofer
    ln -s build/falcon/falcon
    ln -s build/qwkie/qwkie
    ln -s build/binki/binki    

## Copy Example Data

### Copy the default INI file

    cp source/talisman/Talisman/Talisman.ini talisman.ini

### Copy the default Menus

    cp source/talisman/Talisman/menus/* menus/

### Copy the defaut Gfiles

    cp -r source/talisman/Talisman/gfiles/* gfiles/

### Copy the default Scripts

    cp source/talisman/Talisman/scripts/* scripts/

### Copy default configuration

    cp source/talisman/Talisman/data/* data/

## Edit talisman.ini

Change the settings to your liking.

## Run Servo

To run the bbs and begin accepting connections, you run servo from the bbs root directory.

    ./servo

Servo will launch talisman when on an incoming call.

