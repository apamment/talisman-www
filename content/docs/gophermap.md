---
title: "Gopher: gophermap"
layout: "Single"
---

A gophermap is a file that is displayed via gopher instead of a directory 
listing, similar to how index.html works in HTTP servers.

Gophermaps are plain text files, however they use a special format. Each
line must be prepended by a character signifying it's "type".

    iThis is an example gophermap replace <TAB> with a tab character
    i
    iInformational Messages are prefixed by the character 'i'
    i
    0Plain Text File<TAB>plain.txt
    iThe above line is a link to a plain text file called plain.txt
    i
    1Some Directory<TAB>somedir
    iThe above line is a link to a directory
    i
    1My Friend's Gopher<TAB><TAB>myfriend.com<TAB>70
    iThe above line is a link to another gopher server

Lines as you can see, are in the format of :

    <CHAR>Description<TAB>SELECTOR<TAB>HOST<TAB>PORT<CRLF>

Where `<CHAR>` is the character representing type, SELECTOR is the "file"
to access, HOST is the hostname of the server and CRLF is a carraige 
return / linefeed.

Gofer is smart enough to fill in missing fields with defaults.

The following characters are used:

    i - Informational Message
    0 - Plain Text file
    1 - Directory
    2 - CSO search query
    3 - Error message
    4 - BinHex encoded text file
    5 - Binary (PC-DOS) archive file
    6 - UUEncoded text file
    7 - Search engine query
    8 - Telnet session pointer
    9 - Binary file
    g - GIF image file
    h - HTML file
    I - Image file (other than GIF)
    s - Audio file
    P - PDF file
    M - MIME encoded message
    ; - Video file


Gophermaps can contain embedded directory listings, using

    %FILES%

Gophermaps can also display Comments, with lines prefixed by a #.

    # This is a comment
