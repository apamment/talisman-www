---
title: "talisman.ini Documentation"
layout: "single"
---

talisman.ini exists in the bbs root directory. It contains information such as paths and basic system information.

## Main Section

 * **hostname** Your publicly visible hostname. *(default: localhost)*
 * **telnet port** This is the port servo will listen for telnet connections on.
 * **ssh port** This is the port servo will listen for ssh connections, optional.
 * **gopher port** This is the port servo will listen for gopher connections, optional.
 * **binkp port** This is the port servo will listen for binkp connections, optional.
 * **max nodes** The total number of nodes you want to support.
 * **root menu** This is the "main menu", the first menu shown after login.
 * **sysop name** This is the sysop's username. It should match the username entered at login for the sysop.
 * **system name** This is your BBS's name.
 * **qwk id** The QWK id of the system, up to 8 characters, UPPERCASE *(default: TALISMAN)*
 * **location** Location of the BBS, used in QWK packets. *(default: "Somewhere, The World")*
 * **new user sec level** Initial security level of a new user *(default: 10)*
 * **new user feedback** If a new user is required to leave feedback or not (true / false) *(default: false)*
 * **main aka** Your main fidonet address *(default: 0:0/0.0)*
 * **input background** The background colour of input prompts *(default: red)*
   * Options:
     * black
     * red
     * blue
     * brown
     * green
     * cyan
     * magenta
     * white
 * **input foreground** The foreground colour of input prompts *(default: bright white)*
   * Options:
     * black
     * red
     * blue
     * brown
     * green
     * cyan
     * magenta
     * white
     * bright black
     * bright red
     * bright blue
     * bright brown
     * bright green
     * bright cyan
     * bright magenta
     * bright white
 * **Enable IPv6** Enable ipv6 support in servo, true or false. *(default: false)*
 * **IP Block Timeout** The number of seconds since the first try to reset the counter *(default: 300)*
 * **IP Block Attempts** The number of attempts that may occur in the timeout specified above, before a block occurs *(default: 5)*
 * **Windows Local Echo** Whether Talisman will echo the screen on the local console, or just an empty console (speeds things up in some cases - no effect on linux) *default: true*
 * **New User Password** The password required to sign up to the bbs, *default: nothing (disabled)*
 
## Paths Section
 
 * **gfiles path** Path to gfiles (ans/asc files).
 * **data path** Path to data files and data bases.
 * **menu path** Path to menu TOML files.
 * **message path** Path to Squish message bases.
 * **temp path** Path to a temporary directory, drop files are stored here
 * **log path** Path to log file directory, logs placed in here.
 * **script path** Path to lua scripts.
 * **netmail semaphore** Path and filename of a semaphore to modify/create when a netmail is posted.
 * **echomail semaphore** Path and filename of a semaphore to modify/create when a echomail is posted.
 * **external editor** A script or batch file that launches the external editor (is passed a node number and on windows a socket handle also).
 * **gopher root** The root directory served via gopher.
