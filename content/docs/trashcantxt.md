---
title: "Forbidden Usernames"
layout: "Single"
---

*trashcan.txt* contains a list of forbidden usernames.

Usernames that should not be allowed include:

  * new
  * sysop
  * unknown

Plus any you think would be inappropriate.

