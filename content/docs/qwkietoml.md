---
title: "Qwkie Config"
layout: "Single"
---

NOTE: Besides qwkie.toml, qwkie also uses [archivers.toml](/docs/archiverstoml) and [talisman.ini](/docs/talismanini), it must be run from the bbs root directory.

*qwkie.toml* contains information for the qwk networking tool "Qwkie".

example:

    [[network]]
    name = "dovenet"
    hostqwkid = "VERT"
    ftpuser = "HAPPYLND"
    ftphost = "vert.synchro.net"
    ftpport = 21
    password = "SECRET"
    archiver = "zip"
    tagline = "HappyLand v2.0 - telnet://happylandbbs.com:11892/"

    [[area]]
    network = "dovenet"
    basenumber = 2001
    msgbase = "dove/general"

    [[area]]
    network = "dovenet"
    basenumber = 2008
    msgbase = "dove/sysop"

    [[area]]
    network = "dovenet"
    basenumber = 2010
    msgbase = "dove/dovesysop"

## The network Array

This array is a list of QWK networks.

 * **name** A name for this network, used in the area array, and on the command line to refer to this network. *Required*
 * **hostqwkid** The QWK id for your host.
 * **ftpuser** The username you use to login your FTP host to fetch your QWK packet.
 * **ftphost** The FTP host you connect to to fetch your QWK packet.
 * **ftpport** The port your FTP host listens on for FTP connections (usually 21)
 * **password** The FTP password used to login to your FTP host.
 * **archiver** The Archiver to use for compressing reply packets.
 * **tagline** The tagline for this network.

## The area Array

This array is all the areas you receive.

 * **network** The name of the network this area belongs to (as specified in the network array).
 * **basenumber** The QWK id for this area.
 * **msgbase** The Squish message base, relative to the *message path* in talisman.ini.


## Qwkie Usage

Qwkie has three commands, *scan*, *poll* and *toss*.

Each command can optionally be postfixed with a network name, otherwise it will do the operation on all networks.

example:

    ./qwkie scan dovenet

A nice script for a network which you can put in your events.toml:

    #!/bin/bash

    NETWORK="dovenet"

    cd /home/user/bbs
    echo "--------------------------------------------------" >> logs/$NETWORK.log
    echo `date` >> logs/$NETWORK.log
    echo "--------------------------------------------------" >> logs/$NETWORK.log
    ./qwkie scan $NETWORK >> logs/$NETWORK.log 2>&1
    ./qwkie poll $NETWORK >> logs/$NETWORK.log 2>&1
    ./qwkie toss $NETWORK >> logs/$NETWORK.log 2>&1
