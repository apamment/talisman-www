---
title: "Blocked IP Addresses"
layout: "Single"
---

*blocklist.ip* contains a list of blocked ips.

IP addresses that hammer the server are added automatically. Hammering is considered connecting 5 times within 5 minutes.