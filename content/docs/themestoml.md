---
title: "Theme Config"
layout: "Single"
---

*themes.toml* contains an array of available themes.


### Example

    [[theme]]
    name = "My Funky Theme"
    gfile_path = "/path/to/themes/funky/gfiles/"
    menu_path = "/path/to/themes/funky/menus/"
    req_ansi = true

### The Attributes

  * **name** The name of the theme. *Optional default: Theme N* (where N = number)
  * **gfile_path** Gfiles for this theme are stored here. *Optional default: global gfile path*
  * **menu_path** Menus for this theme are stored here. *Optional default: global menu path*
  * **req_ansi** If ANSI support is required for this theme. *Optional default: false*
