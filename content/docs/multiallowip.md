---
title: "Multi-allowed IP Addresses"
layout: "Single"
---

*multiallow.ip* contains a list of ips that may login more than once at a time.

Useful for users connecting via another BBS, or telnet gateway.
