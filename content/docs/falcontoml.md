---
title: "Falcon Config"
layout: "Single"
---

NOTE: Besides falcon.toml, falcon also uses  [talisman.ini](/docs/talismanini), it must be run from the bbs root directory.

*falcon.toml* contains information for the wwivnet scanner / tosser falcon.

example:

    [falcon]
    inbound = "/home/andrew/bbs/ftn/in_sec"
    heart_codes = "strip"

    [[network]]
    name = "Rushnet"
    outbox = "/home/andrew/bbs/ftn/ob/rushnet"
    mynode = 101
    uplink = 100
    emailbase = "rush/email"

    [[network]]
    name = "WWIVnet"
    outbox = "/home/andrew/bbs/ftn/ob/wwivnet"
    mynode = 102
    uplink = 1
    emailbase = "wwivnet/email"

    [[area]]
    net = "Rushnet"
    file = "rush/talisman"
    subtype = "TALISMA"
    mynode = 101
    host = 100

    [[area]]
    net = "WWIVnet"
    file = "wwivnet/linwwiv"
    subtype = "LINWWIV"
    mynode = 102
    host = 60

## The [falcon] Section

 * *inbound* this is your protected inbound, falcon looks for mail bundles here.
 * *heart_codes* can be either **strip** or **convert** to either strip heart colour codes, or convert them to pipe codes. Defaults to strip. (Optional)

## The [[network]] Array

 * *name* This is the name of the network, used to identify which areas belong to which network.
 * *outbox* This is the filebox for outbound mail packets.
 * *mynode* Your node number on the network.
 * *uplink* Your uplink's node number.
 * *emailbase* The file name of the squish base to store "email" for this network (relative to your msgs folder).

## The [[area]] Array

 * *net* The "Name" of the network this area belongs to.
 * *file* The file name of the squish base to store messages for this area (relative to your msgs folder).
 * *subtype* The "subtype" of the area. This is similar to the areatag in FTN, and identifies which area a message is for.
 * *mynode* Your node number.
 * *host* The node number of the sub's host.

# BinkD Settings

In addition to setting up falcon, you will also need to set up binkd for sending and receiving packets. WWIVnet uses the fake zone and net numbers 20000, the node number is your wwivnet node number, and point is 0.

You would set up your "address" as 20000:20000/999@wwivnet (if your node number was 999).

A node entry for The Weather Station (The hub for WWIVnet) might look like this:

    node 20000:20000/1@wwivnet -md bbs.weather-station.org:24556 password c /home/andrew/bbs/ftn/ob/wwivnet

Replace "password" with your binkp password, and the path at the end with the path of your filebox for outbound packets.

WWIV sends packets in the format of sNN.net where NN is your node number. So, if you are node number 999 you would receive packets named s999.net. You will want to process them as soon as they arrive, else binkd will rename them and falcon won't see them.

So, something like:

    exec "/home/andrew/bbs/falcon_toss.sh" *.net

Will kick of a script for all .net files. You can replace this with s999.net, though s*.net didn't seem to work for me.

Falcon should be run from the BBS directory, so use a script to call it.. eg

    #!/bin/bash

    cd /home/andrew/bbs
    ./falcon toss

# Subscribing / Dropping Subs

To actually subscribe (or drop) a sub, you will need to send a special message to the host of the sub. Falcon does this for you :

    ./falcon add WWIVnet 1 WEATHER

This will send a subscribe message on the network named WWIVnet to the host (in this case node 1) a request to join the WEATHER sub.

Similarly, 

    ./falcon drop WWIVnet 1 WEATHER

Will send a drop (unsubscribe) message.

You should receive an "Email" in your "emailbase" with the result of the subscribe or drop request. If it is successful, then add or remove the area to your config files.

