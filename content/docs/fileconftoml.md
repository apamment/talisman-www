---
title: "File Conference Config"
layout: "single"
---

*fileconfs.toml* contains an array of File Conference definitions.


### Example

    [[fileconf]]
    name = "General Files"
    config = "fb_confx"
    sec_level = 10


### The Attributes

  * **name** This is the name of the conference as displayed to users. *Required*
  * **config** This is the name of the config file (relative to the data path) that contains the [file area definitions](/docs/fbconfxtoml). *Required*
  * **sec_level** This is the security level required to access the conference *Optional (default 10)*

