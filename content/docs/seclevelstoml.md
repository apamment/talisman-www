---
title: "Security Level Config"
layout: "Single"
---

*seclevels.toml* contains an array of security level definitions.


### Example

    [[seclevel]]
    name = "Standard User"
    sec_level = 10
    mins_per_day = 120
    timeout_mins = 10
    bulk_msg_allowed = false
    can_delete_own_msgs = false
    can_delete_msgs = false
    invisible = false

### The Attributes

  * **name** A name to define the security level. *Optional default: Unknown Name*
  * **sec_level** The security level being defined. *Required*
  * **mins_per_day** The number of minutes per day a user on this security level is granted *Required*
  * **timeout_mins** The number of minutes a user can idle for before they are disconnected *Required*
  * **bulk_msg_allowed** If a user of this security level is allowed to send bulk mail via the index reader *Optional default: false*
  * **can_delete_own_msgs** If a user of this security can delete messages in ANY AREA that they have written. *Optional default: false*
  * **can_delete_msgs** If a user of this security can delete ANY MESSAGE in ANY AREA. *Optional default: false*
  * **invisible** If a user of this security should be considered invisible to call logs. *Optional default: false*
  
