---
title: "Archiver Config"
layout: "Single"
---

*archivers.toml* contains an array of archiver definitions.


### Example

    [[archiver]]
    name = "Zip"
    extension = ".zip"
    unarc = "/usr/bin/unzip -jLoq -d @OUTDIR@ @ARCHIVE@ @FILELIST@"
    arc = "/usr/bin/zip -jkq @ARCHIVE@ @FILELIST@"
    signature = "504b0304"
    offset = 0



### The Attributes

  * **name** This is the name of the archiver as displayed to users. *Required*
  * **extension** This is the extension archives of this type will be identified by requires the leading dot. *Required*
  * **unarc** This is the command to decompress the archive, *Required* Macros include:
    * @OUTDIR@ The directory to place the unarchived files.
    * @ARCHIVE@ The archive name.
    * @FILELIST@ The list of files to get from the archive.
  * **arc** This is the command to compress the archive, *Required* Macros include:
    * @ARCHIVE@ The archive name.
    * @FILELIST@ The list of files to put in the archive.
  * **signature** A signature to identify an archive *Required for postie*
  * **offset** An integer offset of an archive to find the signature, negative from end of file, positive from start *Required for postie*