---
title: "Message Conference Config"
layout: "single"
---

*msgconfs.toml* contains an array of Message Conference definitions.


### Example

    [[messageconf]]
    name = "Local"
    config = "mb_confx"
    sec_level = 10
    tagline = "Another Cool Talisman BBS"

### The Attributes

  * **name** This is the name of the conference as displayed to users. *Required*
  * **config** This is the name of the config file (relative to the data path) that contains the [message area definitions](/docs/mbconfxtoml). *Required*
  * **sec_level** This is the security level required to access the conference *Optional (default 10)*
  * **tagline** This is the tagline displayed on network messages *Optional (default A Mysterious BBS)*

