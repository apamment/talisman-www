---
title: "Lua Scripting Commands"
layout: "Single"
---
## Special Scripts

 * **login.lua** This script if it exists can replace the standard telnet login. It should have one function "login" that returns the username and password.
 * **newuser.lua** This script if it exists is run after a new user account is created and saved.
 * **prelogin.lua** This script if it exists runs before a user logs in. It should have one function "prelogin" and it should return 1 to continue with login or 0 to disconnect.
 * **predoor.lua** This script if it exists is run before a door is run with RUNDOOR.

## Talisman Specific Lua Commands

 * **bbs_write_string** displays a string to the user (argument: a string);
 * **bbs_read_string** reads a string from the user (argument: max length) (returns a string).
 * **bbs_read_password** reads a masked string from the user (argument: max length) (returns a string).
 * **bbs_getchar** reads a single character from the user. (argument: none) (returns a string).
 * **bbs_get_data_path** returns the BBS script data path (this is a data folder in the scripts_path). (argument: none) (returns a string)
 * **bbs_display_gfile** displays a gfile to the user. (argument: string)
 * **bbs_display_gfile_pause** displays a gfile to the user with page pausing. (argument: string)
 * **bbs_get_username** gets the user's username. (argument: none) (returns a string)
 * **bbs_clear_screen** clears the screen (on ANSI terminals) (argument: none)
 * **bbs_post_message** Posts a message to an area (arguments: message base file, to, from, subject, body)
 * **bbs_get_message** Gets a message from an area (arguments: message base file, msgid) (returns: actual msgid, to, from, subject, body)
 * **bbs_get_user_location** Gets the user's location (arguments: none) (returns: location as a string)
 * **bbs_get_os** Gets the name of the operating system Talisman is running on (arguments: none) (returns: os name as a string)
 * **bbs_pause** Displays a pause prompt (argument: none)
 * **bbs_get_user_attribute** Gets a specified user attribute (arguments: attribute string, default string)
 * **bbs_get_calllog_x** Get the Call log for the X most recent call (arguments: x integer where 0 = last call, 1 = second last call...) (returns: call number, node, username, timeon, timeoff, upload, download, msg posts, doors run)
 * **bbs_user_get_total_calls** Get the total calls for user (arguments: username string) (returns: total calls)
 * **bbs_user_get_total_uploads** Get the total uploads (in bytes) for user (arguments: username string) (returns: upload total)
 * **bbs_user_get_total_downloads** Get the total downloads (in bytes) for user (arguments: username string) (returns: download total)
 * **bbs_user_get_total_msgposts** Get the total msg posts for user (arguments: username string) (returns: msgpost total)
 * **bbs_user_get_total_doorsrun** Get the total doors run for user (arguments: username string) (returns: doors run total)
 * **bbs_get_total_calls** Get the total calls for bbs (arguments: none) (returns: total calls)
 * **bbs_get_total_uploads** Get the total uploads (in bytes) for bbs (arguments: none) (returns: upload total)
 * **bbs_get_total_downloads** Get the total downloads (in bytes) for bbs (arguments: none) (returns: download total)
 * **bbs_get_total_msgposts** Get the total msg posts for bbs (arguments: none) (returns: msgpost total)
 * **bbs_get_total_doorsrun** Get the total doors run for bbs (arguments: none) (returns: doors run total)
 * **bbs_user_has_ansi** Check if the user has ANSI support (arguments: none) (returns: true or false)
 * **bbs_set_user_attribute** Set a user attribute, an attribute has a key (string) and a value (string)
 * **bbs_edit_ansi** Launches the built in ANSI editor, (arguments: width, height, full path and filename)
 * **bbs_upload** Allow the user to upload, (arguments: none) (returns: table of all uploaded files)
 * **bbs_download** Allow the user to download a file (arguments: full path and filename of the file to download)
 * **bbs_rlogin_ip4** Rlogin to a remote server vi IPv4, (arguments: host, port, local username, remote username, term type)
 * **bbs_rlogin_ip6** Rlogin to a remote server vi IPv6, (arguments: host, port, local username, remote username, term type)
 * **bbs_telnet_ip4** Telnet to a remote server vi IPv4, (arguments: host, port)
 * **bbs_telnet_ip6** Telnet to a remote server vi IPv6, (arguments: host, port)
 * **bbs_get_bbs_name** Get the name of the BBS, (arguments: none) (returns: string)
 * **bbs_get_sysop_name** Get the name of the Sysop of the BBS, (arguments: none) (returns: string)
 * **bbs_get_user_attribute_by_name** Gets a specified user attribute for a user other than the one logged in (arguments: username string, attribute string, default string)
 * **bbs_get_node** Gets the current node number (arguments: none)
 * **bbs_get_message_detail** Gets a particular detail of a message. (arguments: message base file, message number, string representing the detail)  (returns: a string)
   * Details include:
   * ORIGINADDR: returns the origin address of the message
   * LOCAL: returns TRUE or FALSE (as a string) whether the message was entered locally.
 * **bbs_get_top_user** Given an attribute, it gets the user in a given place (ignoring the sysop) attribute values must be integers. (arguments: attribute, place) (returns: the username, the value)
 * **bbs_get_user_ip** Returns the users ip address as a string. (arguments: none)
 * **bbs_get_term_type** Returns the terminal type of the connected user. (arguments: none)
 * **bbs_display_sixel** Displays a sixel if the user's terminal supports it. (arguments: full path and filename of the sixel)
 * **bbs_switch_font** Switches terminal font (if the user's terminal supports it) (arguments: font slot number, place to change)
   * Place to change:
   * 0 - default (applies font to all places including normal)
   * 1 - bold
   * 2 - blink
   * 3 - bold & blink

## Talisman Attributes

The following attributes are used by the BBS and can be gotten or set via **bbs_get_user_attribute** or **bbs_set_user_attribute**.

 * archiver
 * bluewave_pkt_no
 * cur_file_area
 * cur_file_conf
 * cur_msg_area
 * cur_msg_conf
 * email
 * first_on
 * fullname
 * fullscreeneditor
 * fullscreenreader
 * last_on
 * location
 * screen_override
 * screen_override_height
 * screen_override_width
 * seclevel
 * signature
 * signature_enabled
 * theme
 * time_left
 * viewkludges

In order to avoid collisions with future possible BBS attributes, please prefix your script attributes with *script_*, it might also be wise to further prefix with your name, or script name, for example:

    script_apam_some_attribute

Or perhaps,

    script_apam_ibol.lua_some_attribute

Please note: all attributes for a particular user are deleted when a user is deleted.
