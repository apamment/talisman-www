---
title: "Postie Config"
layout: "Single"
---

NOTE: Besides postie.toml, postie also uses [archivers.toml](/docs/archiverstoml) and [talisman.ini](/docs/talismanini), it must be run from the bbs root directory.

*postie.toml* contains information for the scanner / tosser postie.

example:

    [postie]
    inbound = "ftn/in"
    protinbound = "ftn/in_sec"
    outbound = "ftn/out"
    packetdir = "ftn/packets"
    dupebase = "dupes"
     
    [[address]]
    aka = "21:1/126.2"
     
    [[link]]
    aka = "21:1/126"
    ouraka = "21:1/126.2"
    archiver = "zip"
    flavour = "hold"
     
    [[route]]
    aka = "21:1/126"
    pattern = "21:*"
     
    [[area]]
    aka = "21:1/126.2"
    file = "fsx/tst"
    tag = "FSX_TST"
    links = "21:1/126"
     
    [[netarea]]
    aka = "21:1/126.2"
    file = "fsx/netmail"

    [[filearea]]
    aka = "21:1/126.2"
    directory = "dloads/fsx/node"
    database = "fb_fsxnode"
    tag = "FSX_NODE"
    links = "21:1/126"
    hook = "/home/pi/bbs/fsx_node_hook.sh"

## The [postie] Section

 * *inbound* this is your unprotected inbound, postie looks for netmails in here.
 * *protinbound* this is your secure inbound, postie looks for bundles and packets in here.
 * *outbound* your outbound directory, where FLO files etc are put.
 * *packetdir* a directory where outbound bundles are stored.
 * *dupebase* a message base for storing duplicate messages.
 * *bundlename* the type of name given to a bundle can be "timestamp" or "nodediff" (Optional)
 
## The [[address]] Array

This array is a list of your addresses.

 * *aka* Your address

NOTE: A common misconception is that more than one aka can follow an [[address]] tag,

The correct way to define multiple addresses is:

    [[address]]
    aka = "21:1/999"

    [[address]]
    aka = "637:1/999"


## The [[link]] Array

This array is a list of your uplinks / downlinks.

 * *aka* The address for the link
 * *ouraka* Your address for the link, should be in [[address]] array.
 * *archiver* The NAME of the archiver as defined in [archivers.toml](/docs/archiverstoml)
 * *flavour* Whether this link should be CRASH, HOLD or NORMAL
 * *filebox* A filebox for this node (Used to send TIC files to downlinks)
 * *packetpwd* Packet password (Optional)
 * *areafixpwd* Password for areafix. (Optional)
 * *ticpwd* Password for tic files. (Optional)
 * *allowedgroups* String of group characters a link is allowed to join / remove. (Optional)
 
## The [[route]] Array

This array stores routing information for netmail. Postie requires all netmail to be routed
through one of your links.

 * *aka* The address for the link to route through
 * *pattern* The pattern to match, * is a wildcard. 

## The [[area]] Array

This array is where you store information about your echomail message bases.

 * *aka* Your address for this base.
 * *file* The file name (without extension) of the squish base relative to the message base directory.
 * *tag* The echomail AREA tag / name.
 * *links* A comma seperated list of links attached to this area.
 * *group* A character representing the group this area belongs to (Don't use '0') (Optional)
 * *scan_hook* A script to execute when a message is scanned out of this area, the hook is passed AREA tag, Subject, Author (Optional)
 * *toss_hook* A script to execute when a message is tossed in to this area, the hook is passed AREA tag, Subject, Author (Optional)

## The [[netarea]] Array

This array is where you define your netmail areas. Talisman / Postie uses 1 netmail area per network.

 * *aka* Your address for this base.
 * *file* The file name (without extension) of the squish base relative to the message base directory.

## The [[filearea]] Array

This array is where you define file areas.

 * *aka* Your address for this base.
 * *directory* The directory which to put the files in.
 * *database* The name of the filebase's database.
 * *tag* The file area tag.
 * *links* A comma seperated list of links attached to this area, links can be prefixed with a special character to indicate link type.
   * *&* Indicates processing is allowed, and forwarding is allowed
   * *=* Indicates that processing is not allowed, forwarding is allowed (ie, pass through)
   * *!* No processing and No forwarding. (ie downlink only)
   * *No prefix* Indicates that processing is allowed, forwarding is not allowed.
 * *hook* A script to execute when a file is received in this area (Optional)

## See Also

 * [events.toml](/docs/eventstoml) for an example of configuring servo to automatically scan mail out.
